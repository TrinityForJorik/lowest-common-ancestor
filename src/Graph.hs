module Graph where

import Data.List (intersect)

data Graph a = GNode a [Graph a] | Nil deriving (Eq, Show)

instance Functor Graph where
  fmap f Nil = Nil
  fmap f (GNode a as) = GNode (f a) $ fmap (fmap f) as

findPath :: Eq a => a -> Graph a -> [a]
findPath = findPath' []

findPath' :: Eq a => [a] -> a -> Graph a -> [a]
findPath' p m Nil = []
findPath' p m (GNode n ns)
  | n == m = m:p
  | otherwise = if null filteredList then [] else head filteredList
  where
    filteredList = filter (not . null) findInChildren
    findInChildren = map (findPath' (n:p) m) ns

exists :: Eq a => Graph a -> a -> Bool
exists Nil _ = False
exists (GNode n ns) m =
  n == m || any existsInChildren ns
  where
    existsInChildren = flip exists m

lcaGraph :: Eq a => Graph a -> a -> a -> Maybe a
lcaGraph g n m = if null ancestor then Nothing else Just $ head ancestor
  where
    pathInGraph node = findPath node g
    ancestor = pathInGraph n `intersect` pathInGraph m
