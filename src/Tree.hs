module Tree where

import Data.Monoid
import Data.List hiding (delete)

data Tree a = Node a (Tree a) (Tree a) | Empty

newNode :: Tree Int
newNode =
  Node 4
  (Node 3
    (Node 2 Empty Empty)
    Empty)
  (Node 5
    Empty
    (Node 6 Empty Empty))

instance Show a => Show (Tree a) where
  show tree = tail $ showWithDepth tree 0
    where
      showWithDepth :: Show a => Tree a -> Int -> String
      showWithDepth Empty depth = '\n' : replicate depth ' ' <> "Empty"
      showWithDepth (Node a left right) depth = 
        '\n' : replicate depth ' ' <> show a <>
          showWithDepth left (depth + 4) <>
            showWithDepth right (depth + 4)

instance Eq a => Eq (Tree a) where
  Empty == Empty = True
  Empty == _ = False
  _ == Empty = False
  Node c l r == Node c' l' r' = c == c' && l == l' && r == r'

member :: Ord a => a -> Tree a -> Bool
member n Empty = False
member n (Node c l r) = c == n || member n l || member n r

smallestElement :: Tree a -> a
smallestElement (Node a Empty _) = a
smallestElement (Node _ l _) = smallestElement l

treeFromList :: Ord a => [a] -> Tree a
treeFromList [] = Empty
treeFromList n = foldl' (flip add) Empty n

add :: Ord a => a -> Tree a -> Tree a
add n Empty = Node n Empty Empty
add n (Node c l r)
  | n == c = Node n l r
  | n < c = Node c (add n l) r
  | n > c = Node c l (add n r)

delete :: Ord a => a -> Tree a -> Tree a
delete _ Empty = Empty
delete n node@(Node c l r)
  | c > n = Node c (delete n l) r
  | c < n = Node c l (delete n r)
  | c == n = deleteNode node
  where
    deleteNode (Node a Empty r) = r
    deleteNode (Node a l Empty) = l
    deleteNode (Node a l r) = Node (smallestElement r) l r

pathToNode :: Ord a => a -> Tree a -> [a]
pathToNode n Empty = []
pathToNode n tree =
  if member n tree then path else []
  where
    path = probablePathToNode n tree
    probablePathToNode n (Node c l r)
      | n == c = [c]
      | n < c = c : probablePathToNode n l
      | n > c = c : probablePathToNode n r

lowestCommonAncestor :: Ord a => a -> a -> Tree a -> Maybe a
lowestCommonAncestor fst snd tree =
  if not $ memberTree fst && memberTree snd
    then Nothing
  else Just $ last pathBetweenNodes
    where
      memberTree = flip member tree
      pathInTree = flip pathToNode tree
      pathBetweenNodes = pathInTree fst `intersect` pathInTree snd
