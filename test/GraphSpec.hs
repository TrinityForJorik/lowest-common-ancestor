module Main where

import Test.HUnit
import Test.Framework as TF (defaultMain, testGroup, Test)
import Test.Framework.Providers.HUnit (testCase)

import Graph

main = defaultMain tests

tests = [ testGroup "Test the API for Graph"
          [
            graphFindPath
            , graphExists
            , graphLowestCommonAncestor
          ]
        ]

emptyGraph = Nil
smallGraph = 
  (GNode 4
    [GNode 5 [Nil],
      GNode 6 [GNode 5 [GNode 91 [GNode 100 [Nil],
      GNode 101 [GNode 900 [GNode 1000 [Nil],
      GNode 1001 [Nil]]]]]]]) :: Graph Integer

graphExists =
  testGroup "Test operation of checking existence of nodes in a Graph"
  [
    testCase "Check if root of graph exists" (exists smallGraph 4 @?= True)
    , testCase "Check for a non-existenet graph"
      (exists smallGraph 87 @?= False)
    , testCase "Check for existence in a empty Graph" 
      (exists emptyGraph 5 @?= False)
    , testCase "Check for existence of nodes in a biggish graph"
      (exists smallGraph 1000 @?= True)
  ]

graphFindPath =
  testGroup "Test operation of find path to nodes in a Graph"
  [
    testCase "Check for a path to a non-existing node"
      (findPath 99 smallGraph @?= [])
    , testCase "Check for a path to the root in graph"
      (findPath 4 smallGraph @?= [4])
    , testCase "Check path for an arbitary node in graph"
      (findPath 91 smallGraph @?= [91,5,6,4])
  ]

graphLowestCommonAncestor =
  testGroup "Test operation of finding lowest common ancestor"
  [
    testCase "Check for lowest common ancestor of the same node"
      (lcaGraph smallGraph 91 91 @?= Just 91)
    , testCase "Check for lowest common ancestor of root and an arbitarty node"
      (lcaGraph smallGraph 1001 4 @?= Just 4)
    , testCase "Check for lowest common ancestor of two arbitary nodes"
      (lcaGraph smallGraph 1001 100 @?= Just 91)
  ]
