module TreeSpec where

import Test.HUnit
import Test.Framework as TF (defaultMain, testGroup, Test)
import Test.Framework.Providers.HUnit (testCase)

import Tree

main = defaultMain tests

tests = [ testGroup "Test the API for Tree"
          [
            treeAdd
            , treeDelete
            , treePathToNode
            , treeLowestCommonAncestor
          ]
        ]

treeAdd =
  testGroup "Test operation of adding to a Tree" 
  [
      testCase "Add to a non-empty tree" 
        (add 3 (Node 4 Empty Empty) @?= (Node 4 (Node 3 Empty Empty) Empty))
    , testCase "Add to an empty tree" (add 5 Empty @?= Node 5 Empty Empty)
    , testCase "Add a duplicate node"
      (add 5 (Node 5 Empty Empty) @?= Node 5 Empty Empty)
  ]

treeDelete =
  testGroup "Test operation of deleting from a Tree" 
  [
      testCase "Delete from an empty tree" (delete 5 Empty @?= Empty)
    , testCase "Delete from a non-empty tree"
        (delete 5 (Node 6 (Node 5 Empty Empty) Empty) @?= (Node 6 Empty Empty))
    , testCase "Delete the root node"
        (delete 6 (Node 6 (Node 5 Empty Empty) Empty) @?= (Node 5 Empty Empty))
  ]

treePathToNode =
  testGroup "Test function to calculate path from the \
    \root of a tree to the given node"
    [
        testCase "Test path to a non-existing node"
        (pathToNode 5 (Node 7 (Node 6 Empty Empty) Empty) @?= [])
      , testCase "Test path to a existing node"
        (pathToNode 5 (Node 7 (Node 6 (Node 5 Empty Empty) Empty) Empty) @?=
        [7, 6, 5])
    ]

treeLowestCommonAncestor =
  testGroup "Test function to calculate the \
    \lowest common ancestor of two nodes"
    [
        testCase "Test common ancestor for pair of nodes that don't exist"
        (lowestCommonAncestor 8 9 (Node 6 (Node 5 Empty Empty) (Node 10 Empty Empty))
          @?= Nothing
        )
      , testCase "Test common ancestor for nodes that exist"
        (lowestCommonAncestor 9 13
        (Node 19
          (Node 10
            (Node 9 Empty Empty)
            (Node 13 Empty Empty))
          Empty) @?= Just 10)
    ]
